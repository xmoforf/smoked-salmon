# Use an official Python runtime as a base image
FROM python:3.9-bookworm

# Set the working directory in the container
WORKDIR /app

# Copy the project files into the container
COPY . /app


# Install the project and its dependencies
RUN apt-get update
RUN apt-get install -y make build-essential libssl-dev zlib1g-dev libbz2-dev libreadline-dev libsqlite3-dev curl libncursesw5-dev xz-utils tk-dev libxml2-dev libxmlsec1-dev libffi-dev liblzma-dev
RUN apt-get install -y sox flac mp3val git wget curl
RUN pip install -U pip setuptools wheel
RUN pip install -e .

# Set the entrypoint to run the 'salmon' script
ENTRYPOINT ["salmon"]

