import os

# Read what each of these settings do here:
# https://github.com/ligh7s/smoked-salmon/wiki/configuration
# There are many optional settings you can also set.

# Required
DOWNLOAD_DIRECTORY    = os.environ.get('DOWNLOAD_DIRECTORY','/downloads')
DOWNLOAD_QUEUE_FOLDER = os.environ.get('DOWNLOAD_QUEUE_FOLDER', '/queue')
DOTTORRENTS_DIR       = os.environ.get('DOTTORRENTS_DIR', '/dottorrents')
RED_SESSION     = os.environ.get('RED_SESSION', 'get-from-site-cookie') #Required for now. (waiting on reports api support)
OPS_SESSION     = os.environ.get('OPS_SESSION', 'get-from-site-cookie')
PTPIMG_KEY            = os.environ.get('PTPIMG_KEY', 'ptpimg-key')
DISCOGS_TOKEN         = os.environ.get('DISCOGS_TOKEN', 'discogs-token')

# Tracker
'''If a default tracker is not set and you have more than one tracker in TRACKER_LIST
then you will be prompted to choose each time you use the script.'''
TRACKER_LIST    = os.environ.get('TRACKER_LIST','RED,OPS').split(',') #Remove one of these if you don't want multi-tracker support.
DEFAULT_TRACKER = os.environ.get('DEFAULT_TRACKER', 'RED')
RED_API_KEY     = os.environ.get('RED_API_KEY', 'red-api-key') #Needs uploading privileges. Optional for now. Some things still use session.
OPS_API_KEY     = os.environ.get('OPS_API_KEY', 'ops-api-key') #Needs uploading privileges. Optional for now. Some things still use session.
MULTI_TRACKER_UPLOAD = os.environ.get('MULTI_TRACKER_UPLOAD', 'True')
RED_DOTTORRENTS_DIR = os.environ.get('RED_DOTTORRENTS_DIR', DOTTORRENTS_DIR)
OPS_DOTTORRENTS_DIR = os.environ.get('OPS_DOTTORRENTS_DIR', DOTTORRENTS_DIR)

# General
CHECK_REQUESTS = os.environ.get('CHECK_REQUESTS', 'True')
ALWAYS_ASK_FOR_REQUEST_FILL = os.environ.get('ALWAYS_ASK_FOR_REQUEST_FILL', 'False')
CHECK_RECENT_UPLOADS = os.environ.get('CHECK_RECENT_UPLOADS', 'True')
LOG_DUPE_TOLERANCE = float(os.environ.get('LOG_DUPE_TOLERANCE', 0.5))
COPY_UPLOADED_URL_TO_CLIPBOARD = bool(os.environ.get('COPY_UPLOADED_URL_TO_CLIPBOARD', False))

# Naming files and folder
FOLDER_TEMPLATE = os.environ.get('FOLDER_TEMPLATE','{artists} - {title} ({year}) [{source} {format}] {{{label}}}')
FILE_TEMPLATE = os.environ.get('FILE_TEMPLATE','{tracknumber}. {artist} - {title}')
NO_ARTIST_IN_FILENAME_IF_ONLY_ONE_ALBUM_ARTIST = bool(os.environ.get('NO_ARTIST_IN_FILENAME_IF_ONLY_ONE_ALBUM_ARTIST', True))
ONE_ALBUM_ARTIST_FILE_TEMPLATE = os.environ.get('ONE_ALBUM_ARTIST_FILE_TEMPLATE', '{tracknumber}. {title}')
LOWERCASE_COVER = bool(os.environ.get('LOWERCASE_COVER', False))
VARIOUS_ARTIST_THRESHOLD = int(os.environ.get('VARIOUS_ARTIST_THRESHOLD', 4))
VARIOUS_ARTIST_WORD = os.environ.get('VARIOUS_ARTIST_WORD','Various')
GUESTS_IN_TRACK_TITLE = bool(os.environ.get('GUESTS_IN_TRACK_TITLE', False))
BLACKLISTED_SUBSTITUTION = os.environ.get('BLACKLISTED_SUBSTITUTION','_')
FULLWIDTH_REPLACEMENTS = bool(os.environ.get('FULLWIDTH_REPLACEMENTS', False))
BLACKLISTED_GENRES = set(os.environ.get('BLACKLISTED_GENRES','Soundtrack').split(','))
REVIEW_AS_COMMENT_TAG = bool(os.environ.get('REVIEW_AS_COMMENT_TAG', True))
STRIP_USELESS_VERSIONS = bool(os.environ.get('STRIP_USELESS_VERSIONS', True))
ADD_EDITION_TITLE_TO_ALBUM_TAG = bool(os.environ.get('ADD_EDITION_TITLE_TO_ALBUM_TAG', True))

# Uploading Process Specific
FLAC_COMPRESSION_LEVEL = int(os.environ.get('FLAC_COMPRESSION_LEVEL', 8))
NATIVE_SPECTRALS_VIEWER = bool(os.environ.get('NATIVE_SPECTRALS_VIEWER', False))
PROMPT_PUDDLETAG = bool(os.environ.get('PROMPT_PUDDLETAG', False))
FEH_FULLSCREEN = bool(os.environ.get('FEH_FULLSCREEN', True))
COMPRESS_SPECTRALS = bool(os.environ.get('COMPRESS_SPECTRALS', False))

# Multithreading
SIMULTANEOUS_SPECTRALS = int(os.environ.get('SIMULTANEOUS_SPECTRALS', 3))
SIMULTANEOUS_CONVERSIONS = int(os.environ.get('SIMULTANEOUS_CONVERSIONS', 2))

# Tag and release searching
SEARCH_LIMIT = int(os.environ.get('SEARCH_LIMIT', 3))
TIDAL_SEARCH_REGIONS = list(os.environ.get('TIDAL_SEARCH_REGIONS', 'DE,NZ,US,GB').split(','))
TIDAL_FETCH_REGIONS = list(os.environ.get('TIDAL_FETCH_REGIONS', '').split(','))
USER_AGENT = os.environ.get('USER_AGENT', 'salmon uploading tools, maintained by lights')
SEARCH_EXCLUDED_LABELS = set(os.environ.get('SEARCH_EXCLUDED_LABELS', '').split(','))

# Description preferences
TRACKLIST_IN_T_DESC = bool(os.environ.get('TRACKLIST_IN_T_DESC', False))
BITRATES_IN_T_DESC = bool(os.environ.get('BITRATES_IN_T_DESC', False))
USE_UPC_AS_CATNO = bool(os.environ.get('USE_UPC_AS_CATNO', True))
ICONS_IN_DESCRIPTIONS = bool(os.environ.get('ICONS_IN_DESCRIPTIONS', False))

# Image uploading
IMAGE_UPLOADER = os.environ.get('IMAGE_UPLOADER', 'ptpimg')
COVER_UPLOADER = os.environ.get('COVER_UPLOADER', 'ptpimg')
SPECS_UPLOADER = os.environ.get('SPECS_UPLOADER', 'emp')
IMGUR_CLIENT_ID = os.environ.get('IMGUR_CLIENT_ID', None)

# Webserver
WEB_HOST = os.environ.get('WEB_HOST', 'http://0.0.0.0:55110')
WEB_PORT = int(os.environ.get('WEB_PORT', 55110))
WEB_STATIC_ROOT_URL = os.environ.get('WEB_STATIC_ROOT_URL', '/static')

